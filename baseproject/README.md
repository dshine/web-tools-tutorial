# Overview

This is a base project for landing pages. It has the following gem's installed 

* [Bootstrap](https://github.com/twbs/bootstrap-rubygem)
* [Fontawesome](https://github.com/FortAwesome/font-awesome-sass)
* [Mailchimp](https://bitbucket.org/mailchimp/mailchimp-api-ruby/)
	* This needs to be configured more
* [Google Analytics](https://github.com/bgarret/google-analytics-rails)
* [Piwik Analytics](http://piwik.org/blog/2012/10/integrate-piwik-into-your-rails-application/)
* [Meta Tags](https://github.com/lassebunk/metamagic)