# Intro
## The Big Plan
* Get the RoR/Frauenloop courses recognized by the job center
* Certificate of completion for the course
* Set up a company/collective/entity 
* Partner with some large companies to get some projects
* Blow them away with how awesome and professional you are
* After period of time you become so indespensible to the company that they want to hire you full time.
* Travel the world as a IT consultant
* Solve world peace
* Retire to your own private island
## Why we are running this extra class?
* Build on your current knowledge. You already know
	* HTML
	* CSS
	* Bootstrap
	* JS
	* Ruby
* RoR course is an introduction course and many of the jobs that we will do will need to go beyond the current course outline. 
* Introduce you to some tools
* Give you a crash course on some technologies that you might not be aware of. 
## What you are going to be working on for projects 
 * Landing Pages 
 * Analytics 
 * Mobile optimizing UX
 * eCommerce App

## Landing Pages
* Often called Landing Page, Squeeze Page, Splash Page or Lead Page. 
	* Single page website
	* Product/App/Company Pre Launch
	* SEO optimized Information page (Product, Person, Company, Event, Band, Movie, Book)
	* Kickstarter or other funding page
	* Realestate/Property page
	* Possibilities are endless
* [Examples](LandingPages) that I "borrowed" from Instapage website. Please review all of these
* Pages normally have CTA (call to action)
	* Sign up to our mailing list
	* Like our Facebook page
	* Follow us on Twitter
	* Share using instagram tag
	* Fill in your details (name, address, telephone number,etc.)
	* Schedule a time for a call/demo 
	* Download our app

## Analytics
* What are analytics
* Who uses it 
* Why do they use it
* What information can you get about users 
	* Browser
	* Language
	* Desktop v Mobile
	* Landing Page
	* Time spent on site
	* Exit pages
	* Location of visitors
* [Analytics Screen Shots](AnalyticsScreenShots)
* How does this help business
* How does it work
* What will you be doing
	* Setup and install
	* Read and analyize reports
	* Translate the data into chunks the customer will understand
	* Recommend where to invest their money on advertizing

## Mobile optimizing UX
* What is UX 
* Change from traditional viewing sites on desktop.
* Different screensizes different presentation.
* Not all sites are optimized for mobile.
* Some are done really badly
* Many clients don't know how their users are accessing their site. 
* Frameworks
	* Bootstrap
	* Foundation 
	* Materialize
	* Skeleton
	* tuktuk
* Advantages and disadvantages of using frameworks
	* Advantages
		* Speeds up the mock-up process
		* Clean and tidy code
		* Solutions to common CSS problems
		* Browser compatibility
		* Learn good practices
		* Having a single procedure to resolve common problems makes maintaining various projects more straightforward.
		* Helpful in collaborative work
	* Disadvantages
		* Mixes content and presentation
		* Unused code leftover
		* Slower learning curve
		* You don’t learn to do it yourself
* Why use a framework?
	* Reinvent the wheel
	* DRY
	* KISS 

![quote](http://quotography.com/i/lazy-bill-gates-7298367009.jpg "Always hire a lazy person")

* Optimize or Rebuild? 
	* What is the Budget
	* Depends on Requirements
	* Analytics Data

## eCommerce App
* What is eCommerce
* Platforms
	* Shopify
	* Woocommerce
	* Magento
	* Prestashop
	* Drupal Commerce

# Browser Plugins
* [Builtwith](https://chrome.google.com/webstore/detail/builtwith/hleikeoncmpohnmdkkagcbppgdecoiak?hl=en) 
* [SEO Plugin](https://chrome.google.com/webstore/detail/seo-meta-in-1-click/bjogjfinolnhfhkbipphpdlldadpnmhc?hl=en)
* [Nimbus](https://chrome.google.com/webstore/detail/nimbus-screenshot-and-scr/bpconcjcammlapcogcnnelfmaeghhagj)
* [Last Pass](https://chrome.google.com/webstore/detail/lastpass-free-password-ma/hdokiejnpimakedhajhdlcegeplioahd)
* [Toogl](https://chrome.google.com/webstore/detail/toggl-button-productivity/oejgccbfbmkkpaidnkphaiaecficdnfn?hl=en)

# Tools
## Dropbox
* [Dropbox.com](http://www.dropbox.com)
* Shared folder in the cloud
* Files added to the shared folder appear on everyone computer
* Useful for dealing with clients
* Handy way to back up important files

## Asana
* [Asana.com](http://www.asana.com)
* Shared to do list
* Task tracking tool 
* Add docs/screen shots
* assign tasks to people
* assign due dates
* comment on a task
* create sections
* add followers to tasks

## Lastpass
* [Create lastpass account](http://www.lastpass.com)
* central storage for all passwords
* share passwords with team members
* sync password changes to the group 

## Timer Tracker
* [Toogl](http://www.toogl.com)
* [Tracker Plugin for Chrome](https://chrome.google.com/webstore/detail/toggl-button-productivity/oejgccbfbmkkpaidnkphaiaecficdnfn?hl=en) 
* Can track time spent on Tasks from right in Asana
* Good to keep a idea of how long you spend on each project. 
* Reporting

## SEO Plugin
* [Install Broswer Plugin](https://chrome.google.com/webstore/detail/bjogjfinolnhfhkbipphpdlldadpnmhc)
* What it shows
* Title
* H1, H2, H3
* Image 
* Keywords
* Description
* links
* missing info that should be there 
* Social media (OpenGraph)
* GTMetrix

## Mailchimp
* [Sign up for mailchimp](http://www.mailchimp.com)
* Collect email addresses
* segmentation of users
* send blast emails
* connect with Google Analytics
* Connect with Social media (Twitter, Facebook)

##Moqups
* [moqups.com](https://moqups.com/)
* Tool for building wire frame of a site
* Why do we mockup?
* Good design process
	* Get requirements
	* Mockup site
	* Design Database
	* Get client approval
	* Build site

## Analytics
* Google Analytics
	* [Create a google analytics account](https://analytics.google.com/analytics/web/)
	* Why does goes google give this for free?
	* [There is a gem for that](https://github.com/bgarret/google-analytics-rails) 
* Piwik analytics
	* Differene between Google and Piwik
	* Email notifications
	* Our Piwik Server (address to follow)
	* [There is a gem for that](http://piwik.org/blog/2012/10/integrate-piwik-into-your-rails-application/)

## Nimbus
* [Browser Plugin for chrome](https://chrome.google.com/webstore/detail/nimbus-screenshot-and-scr/bpconcjcammlapcogcnnelfmaeghhagj)
* Take a screen shot
	* Entire Screen
	* Section 
	* Desktop
* Crop screenshot
* Add highlighted boxes
* Add Notes
* Add sceenshot to a task (asana)
* Add to shared dropbox

## Meta Tags
* What are they
* where are they added to HTML file
* How they impact SEO
* Can they help with Rankings
* Click rate
* Standard Tags
	* Title
	* Description
	* Keywords
	* etc. 
* Guess what, [there is a gem for that](https://github.com/lassebunk/metamagic)

## Open Graph
* What it is used for
* Facebook 
	* Image
	* Text 
* Twitter Cards
	* Image
	* Text
* Guess what, [there is a gem for that](https://github.com/lassebunk/metamagic)
* Read about general [Open Graph](http://ogp.me/) protocol
	* [Facebook Open Graph](https://developers.facebook.com/docs/sharing/webmasters#markup)
	* [Twitter Cards](https://dev.twitter.com/cards/getting-started#opengraph)
	* [Pinterest Rich Pins](https://developers.pinterest.com/docs/rich-pins/reference/)
	* [Google+](https://developers.google.com/+/web/snippet/)


## Builtwith
* [builtwith.com](http://www.builtwith.com)
* [Chrome plugin](https://chrome.google.com/webstore/detail/builtwith/hleikeoncmpohnmdkkagcbppgdecoiak?hl=en)
* Shows the technology that a site uses
	* programming language
	* platform
	* webserver
	* hosting platform

## GTMetrix
* [gtmetrix.com](http://www.gtmetrix.com)
* [Nut&Tea Example](https://gtmetrix.com/compare/1hIMH2rp/vrF1DggZ)
* No plugin but you can use the SEO plugin recommended above
* Page load time
* page size
* what is slowing the page down
	* scripts
	* large images 
	* too many external requests
	* CDN
	* Caching
* recommended changes

## IFTTT
* IF This Then That
* What does it do?
* How can we use this for customer?
* [ifttt.com](https://ifttt.com/channels) 

## Wordpress
* What is a CMS?
* What are a few different CMS solutions
	* Wordpress
	* Drupal
	* Sharepoint
	* Joomla
* Why we are focusing on Wordpress?
* Wordpress Tasks
* Add Media to the library
* Difference between Pages and Posts
* Add 4 pages
	* Home
	* Services
	* About Us
	* Contact Us
* Create a menu and add pages to the menu
* Select and Install a theme
* Configure Theme
* Plugins - What are they?
* Install plugins
	* Yoast SEO
	* Security
	* W3 Total Cache
	* Jetpack
	* Page Builder
	* Comments Evolved
* Edit Theme - custom CSS